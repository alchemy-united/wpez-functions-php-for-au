<?php

namespace WPezFunctionsPhpForAU\App\Core\ImageSizeRegister;

interface InterfaceImageSizeRegister {

	public function registerImageSizes();

}