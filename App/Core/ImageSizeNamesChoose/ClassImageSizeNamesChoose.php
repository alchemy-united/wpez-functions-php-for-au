<?php

namespace WPezFunctionsPhpForAU\App\Core\ImageSizeNamesChoose;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

/**
 * A class of magic for answering questions asked by the filter: 'image_size_names_choose'.
 *
 * Ref: https:// developer.wordpress.org/reference/hooks/image_size_names_choose/ .
 */
class ClassImageSizeNamesChoose implements InterfaceImageSizeNamesChoose {

	/**
	 * TODO.
	 *
	 * @var array
	 */
	protected $arr_imgs;

	/**
	 * TODO.
	 *
	 * @var array
	 */
	protected $arr_defaults;

	/**
	 * TODO.
	 *
	 * @var array
	 */
	protected $arr_defaults_names_choose;

	/**
	 * TODO.
	 *
	 * @var string
	 */
	protected $str_names_choose_add_where;

	/**
	 * TODO.
	 *
	 * @var array
	 */
	protected $arr_names_choose_remove;

	/**
	 * TODO.
	 *
	 * @var integer
	 */
	// protected $int_content_width_count;

	/**
	 * Default is the WP global $content_width.
	 *
	 * @var integer
	 */
	protected $int_content_width;

	/**
	 * TODO.
	 *
	 * @var integer
	 */
	protected $int_content_width_temp;

	/**
	 * TODO.
	 *
	 * @var bool
	 */
	protected $bool_content_width_compare;

	/**
	 * The constructor.
	 */
	public function __construct() {

		$this->setPropertyDefaults();

	}

	/**
	 * Set the various default values for the various properties.
	 *
	 * @return void
	 */
	protected function setPropertyDefaults() {

		global $content_width;

		$this->arr_imgs                   = array();
		$this->arr_defaults               = array(
			'active' => true,
			'name'   => false,
			'width'  => 0,
			'height' => 0,
			'crop'   => false,
		);
		$this->arr_defaults_names_choose  = array(
			'active'                         => true,
			'option'                         => false,
			'content_width_compare_override' => false,
		);
		$this->str_names_choose_add_where = 'after';
		$this->arr_names_choose_remove    = array();
		// $this->int_content_width_count    = 0;
		$this->int_content_width          = $content_width;
		$this->int_content_width_temp     = false;
		$this->bool_content_width_compare = false;
	}

	/**
	 * When we add our array to the array the WP filter passes in, where to we want to add our array.
	 *
	 * @param string $str Expected values: 'before', 'replace', or'after' (default).
	 *
	 * @return void
	 */
	public function setAddWhere( string $str = 'after' ) {

		$this->str_names_choose_add_where = $str;

	}


	/**
	 * Set the property: arr_names_choose_remove.
	 *
	 * @param array $arr Array of value of the img names to be removed from the filter's array.
	 *
	 * @return void
	 */
	public function setRemoveNames( array $arr = array() ) {

		$this->arr_names_choose_remove = $arr;
	}

	/**
	 * Set / override the WP global $content_width.
	 *
	 * Ref: https://codex.wordpress.org/Content_Width
	 * Ref:
	 * https://wycks.wordpress.com/2013/02/14/why-the-content_width-wordpress-global-kinda-sucks/
	 *
	 * @param int $int The content width value
	 */
	public function setContentWidth( int $int = 0 ) {

		$this->int_content_width = absint( $int );
	}

	/**
	 * Note: If set to true, any *custom* (read: add_image_size) image with a
	 * width > $content_width will NOT be added via the image_size_names_choose
	 * filter. The default setting of false will not do any sort of checking.
	 *
	 * @param bool $bool
	 */
	public function setContentWidthCompare( bool $bool = false ) {

		$this->bool_content_width_compare = (bool) $bool;
	}


	/**
	 * Set / update the property: $arr_defaults
	 *
	 * @param array $arr_defaults Array of values to be array_merge()'d with the class'.
	 *
	 * @return void
	 */
	public function updateDefaults( array $arr_defaults = array() ) {

		$this->arr_defaults = array_merge( $this->arr_defaults, $arr_defaults );
	}

	/**
	 * Set / update the property: $arr_defaults_names_choose
	 *
	 * @param array $arr_defaults Array of values to be array_merge()'d with the class'.

	 *
	 * @return void
	 */
	public function updateDefaultsNamesChoose( array $arr_defaults = array() ) {

		$this->arr_defaults_names_choose = array_merge( $this->arr_defaults_names_choose, $arr_defaults );
	}


	/**
	 * Push an image onto the property: $arr_imgs.
	 *
	 * @param array $arr_args The args for the img being pushed on to the array.
	 *
	 * @return void
	 */
	public function pushImage( array $arr_args = array() ) {

		// TODO - Validations?
		$this->arr_imgs[] = $arr_args;
	}


	/**
	 * Wrapper for the method pushImage() to push images in bulk onto the property $arr_imgs.
	 *
	 * @param array $arr_images Array of (multiple) imgs to be pushed onto the array.
	 *
	 * @return array Array of images actually pushed.
	 */
	public function loadImages( array $arr_images = array() ) {

		$arr_ret = array();
		foreach ( $arr_images as $str_ndx => $arr_args ) {
			if ( ! is_array( $arr_args ) ) {
				continue;
			}
			$arr_ret[ $str_ndx ] = $this->pushImage( $arr_args );
		}
		return $arr_ret;
	}


	/**
	 * The callback for the WP filter: 'image_size_names_choose'
	 * Ref: https:// developer.wordpress.org/reference/hooks/image_size_names_choose/
	 *
	 * @param array $arr_sizes Array of image size labels keyed by their name.
	 *
	 * @return array array of image size labels keyed by their name.
	 */
	public function filterImageSizeNamesChoose( array $arr_sizes ) {

		global $content_width;
		static $arr_names_choose;

		// We want this - at least for now - only for the first time this filter fires.
		// if ( 0 === $this->int_content_width_count ) {
		if ( null === $arr_names_choose ) {

			$arr_names_choose = array();

			// $this->int_content_width_count ++;

			$this->int_content_width_temp = $content_width;
			if ( 0 !== $this->int_content_width ) {
				$content_width = $this->int_content_width;
			}

			// Process our new images
			foreach ( $this->arr_imgs as $key => $arr_img ) {

				if ( ! is_array( $arr_img ) ) {
					continue;
				}

				$arr_img = array_merge( $this->arr_defaults, $arr_img );
				if ( false === $arr_img['active'] ) {
					continue;
				}

				if ( isset( $arr_img['names_choose'] ) && is_array( $arr_img['names_choose'] ) ) {
					$arr_img['names_choose'] = array_merge( $this->arr_defaults_names_choose, $arr_img['names_choose'] );

					if ( false === $arr_img['names_choose']['active'] ) {
						continue;
					}
				} else {
					$arr_img['names_choose'] = false;
				}

				/**
				 * Do we want to add this img or not? Check its args for:
				 *
				 * - should we compare the content width?
				 * - ok compare the image width with the content width
				 * - does the image override the content width compare?
				 *
				 * In terms of the magic / benefit of using this class, this is it. Bottom line: define what you want in the array you fed it. It handles the rest. Close to zero actual coding on your part.
				 */
				if ( true === $this->bool_content_width_compare && $arr_img['width'] > $content_width && ( isset( $arr_img['names_choose']['content_width_compare_override'] ) && true !== $arr_img['names_choose']['content_width_compare_override'] ) ) {
					continue;
				}

				// What value are we going to display in the admin UI select? 
				if ( false === $arr_img['names_choose'] || ( isset( $arr_img['names_choose']['option'] ) && ! is_string( $arr_img['names_choose']['option'] ) ) ) {

					// the basic default is to use the image size name.
					$arr_names_choose[ $arr_img['name'] ] = $arr_img['name'];
				} elseif ( is_string( $arr_img['names_choose']['option'] ) ) {

					// if we have a ['names_choose']['option'] then use it!
					$arr_names_choose[ $arr_img['name'] ] = $arr_img['names_choose']['option'];
				}
			}
			// flip the (global) $content_width back to what it was for anything else downstream.
			if ( false !== $this->int_content_width_temp ) {
				$content_width = $this->int_content_width_temp;
			}
		}

		$arr_ret = array();
		switch ( $this->str_names_choose_add_where ) {

			case 'before':
				$arr_ret = array_merge( $arr_names_choose, $arr_sizes );
				break;

			case 'replace':
				$arr_ret = $arr_names_choose;
				break;

			case 'after':
			default:
				$arr_ret = array_merge( $arr_sizes, $arr_names_choose );
				break;
		}

		/*
		 * array = [name1 => bool, name2 => bool,...];
		 * remove if bool === true
		 */
		foreach ( $this->arr_names_choose_remove as $str_name => $bool_active ) {
			if ( true === $bool_active ) {
				unset( $arr_ret[ $str_name ] );
			}
		}

		return $arr_ret;
	}
}
