<?php

namespace WPezFunctionsPhpForAU\App\Core\ImageSizeNamesChoose;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

/**
 * Class for registering hooks The ez Ways. Why write code then you can configure arrays? 
 */
class ClassHooks {

	/**
	 * Instance of the class the hooks are going to "target".
	 *
	 * @var object
	 */
	protected $new_component;

	/**
	 * The default values for the hooks.
	 *
	 * @var array
	 */
	protected $arr_hook_defaults;

	/**
	 * The actions to be hooked.
	 *
	 * @var array
	 */
	protected $arr_actions;

	/**
	 * The filters to be hooked.
	 *
	 * @var array
	 */
	protected $arr_filters;

	/**
	 * The class constructor.
	 *
	 * @param InterfaceImageSizeNamesChoose $obj Instance of a class that implements the InterfaceImageSizeNamesChoose interface.
	 */
	public function __construct( InterfaceImageSizeNamesChoose $obj ) {

		$this->new_component = $obj;
		$this->setPropertyDefaults();
	}

	/**
	 * A method dedicated to setting the initial values of properties.
	 *
	 * @return void
	 */
	protected function setPropertyDefaults() {

		$this->arr_hook_defaults = array(
			'active'        => true,
			'component'     => $this->new_component,
			'priority'      => '10',
			'accepted_args' => '1',
		);

		$this->arr_actions = array();

		/**
		 * Example: $this->arr_actions['after_setup_theme'] = array( 'hook' => 'after_setup_theme', 'callback' => 'TODO', );
		*/
		$this->arr_filters = array();

		$this->arr_filters['image_size_names_choose'] = array(
			'hook'     => 'image_size_names_choose',
			'callback' => 'filterImageSizeNamesChoose',
			'priority' => '100',
		);
	}

	/**
	 * A public method that allows the hook defaults to be updated.
	 *
	 * @param bool $arr Array of new (default) values.
	 *
	 * @return bool
	 */
	public function updateHookDefaults( array $arr = array() ) {

		if ( is_array( $arr ) ) {

			$this->arr_hook_defaults = array_merge( $this->arr_hook_defaults, $arr );
			return true;
		}
		return false;
	}


	/**
	 * Get method to return the current array of actions.
	 *
	 * @return array
	 */
	public function getActions() {

		return $this->arr_actions;
	}



	/**
	 * Set method (sorta) for updating the actions (array).
	 *
	 * @param boolean $arr Array of new actions.
	 *
	 * @return bool
	 */
	public function updateActions( array $arr = array() ) {

		if ( is_array( $arr ) ) {

			$this->arr_actions = array_merge( $this->arr_actions, $arr );
			return true;
		}
		return false;
	}

	/**
	 * Get method to return the array of filters.
	 *
	 * @return array
	 */
	public function getFilters() {

		return $this->arr_filters;
	}

	/**
	 * Set method (sorta) for updating the filters (array).
	 *
	 * @param boolean $arr Array of new filters.
	 *
	 * @return bool
	 */
	public function updateFilters( array $arr = array() ) {

		if ( is_array( $arr ) ) {

			$this->arr_filters = array_merge( $this->arr_filters, $arr );
			return true;
		}
		return false;
	}


	/**
	 * Registers both hook types: add_action and add_filter.
	 *
	 * @param array  $arr_exclude  Array index value (e.g., 'image_size_names_choose') to be excluded from the registration process.
	 * @param array  $arr_hooks    Array of the actions or filters to be registered.
	 * @param string $str_wpfn     WP function to be executed: add_action() or add_filter().
	 *
	 * @return void
	 */
	protected function registerMaster( array $arr_exclude = array(), array $arr_hooks = array(), string $str_wpfn = 'add_action' ) {

		// Validation.
		if ( empty( $arr_hooks ) || ! in_array( $str_wpfn, array( 'add_action', 'add_filter' ) ) ) {
			return;
		}

		foreach ( $arr_hooks as $str_ndx => $arr_hook ) {

			if ( in_array( $str_ndx, $arr_exclude ) ) {
				continue;
			}

			$arr = array_merge( $this->arr_hook_defaults, $arr_hook );
			if ( false === $arr['active'] ) {
				continue;
			}

			if ( 'add_action' === $str_wpfn ) {

				\add_action(
					$arr['hook'],
					array( $arr['component'], $arr['callback'] ),
					$arr['priority'],
					$arr['accepted_args']
				);

			} else {

				\add_filter(
					$arr['hook'],
					array( $arr['component'], $arr['callback'] ),
					$arr['priority'],
					$arr['accepted_args']
				);
			}
		}
	}


	/**
	 * After you instantiate the class, you'll then need to "fire" the register method.
	 *
	 * @param array $arr_exclude Array index value (e.g., 'image_size_names_choose') to be excluded from the registration process. Consider this a last second fail safe.
	 *
	 * @return void
	 */
	public function register( array $arr_exclude = array() ) {

		if ( ! is_array( $arr_exclude ) ) {
			$arr_exclude = array();
		}

		if ( ! empty( $this->arr_actions ) ) {

			$this->registerMaster( $arr_exclude, $this->arr_actions, 'add_action' );
		}

		if ( ! empty( $this->arr_filters ) ) {

			$this->registerMaster( $arr_exclude, $this->arr_filters, 'add_filter' );
		}
	}
}
