## WPezClasses: Images Size Names Choose

__Extends the WPezClasses ImageSizeRegister array standard and does the WordPress filter: 'image_size_names_choose' The ezWay.__

Adds to this select: Image Block > Block Setting (sidebar) > Image Size (select)

If you're not familiar with the filter: 'image_size_names_choose' there's a link to the Codex page below.
   
### Simple Example

_Recommended: Use the WPezClasses autoloader (link below)._

```

use WPezSuite\WPezClasses\ImageSizeNamesChoose\ClassImageSizeNamesChoose as ISNC;
use WPezSuite\WPezClasses\ImageSizeNamesChoose\ClassHooks as Hooks;

$arr_imgs_many = array(
    'size1' => array(
        'name' => 'sqr_md',
        'width' => '500',
        'height' => '500',
        ),   // Note: The default is to add the image to the image block's Image Size select, so we really don't need the 'names_choose' sub-array. 
    'size2' => array(
        'name' => 'sqr_lg',
        'width' => '650',
        'height' => '650'
        'names_choose' => array(
            'active' => false,   // This img size will be registered (if you're also using WPezClasses for registering image sizes), but it won't appear in the image block's Image Size select.
        ),
    ),
);
    
$arr_img_single = array(
    'name' => 'sqr_xl',
    'width' => '800',
    'height' => '800'
    'crop' => true,
    'names_choose' => array(
        'option' => 'Sqr XL',   // Instead of the 'name', use the value of 'option' in the image block's Image Size select.
        'content_width_compare_override' => true,  // Even if this img 'width' is wider than the $content_width, include it. 
    ).
);
    
//
$new_isr = new ISNC();

// load via array
$new_isr->loadImages( $arr_imgs_many );

// or push one at a time
$new_isr->pushImage( $arr_img_single );

// Note: You can also do the hook'ing yourself if you're prefer not to have the convenience of the Hooks class. 
$new_hooks = new Hooks( $new_isr );
$new_hooks->register();

```

Note: The Content Width setting can (automatically) factor in (or not) into which image sizes you wish to add.


### FAQ

__1) Why?__

Traditional WordPress "decouples" adding image sizes (to the image block's Image Size select) from add_image_size(). But the ezWay unites the image definitions for size args and 'image_size_names_choose' into a simple single array. You configure that array, and then a minimal amount of code on your part WPezClasses does all the heavy lifting. 

Less thinking. Less coding. Get more done.

__2) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 


 __3) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 


### HELPFUL LINKS

 - https://gitlab.com/wpezsuite/WPezClasses/ImageSizeRegister

 - https://developer.wordpress.org/reference/hooks/image_size_names_choose/
 
 - https://codex.wordpress.org/Plugin_API/Filter_Reference/image_size_names_choose
 
 - https://codex.wordpress.org/Content_Width
 

### TODO

n/a

### CHANGE LOG

- v0.0.4 - Tuesday 20 October 2022
    - FIXED: Bug in class ClassImageSizeNamesChoose method filterImageSizeNamesChoose()
    - UPDATE: More / better comments and doc comments
    - UPDATE: README

- v0.0.3 - Monday 22 April 2019
    - UPDATED: interface file / name

- v0.0.2 - Saturday 20 April 2019
    - ADDED: ability to remove names from the names choose array

- v0.0.1 - Wednesday 17 April 2019
  - Hey! Ho!! Let's go!!!