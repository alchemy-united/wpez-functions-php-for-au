<?php

namespace WPezFunctionsPhpForAU\App\Core\ImageSizeNamesChoose;

/**
 * Interface that defines the methods that the ClassImageSizeNamesChoose class must implement.
 */
interface InterfaceImageSizeNamesChoose {

	/**
	 * This method will be the callback hooked to the filter: 'image_size_names_choose'. https:// developer.wordpress.org/reference/hooks/image_size_names_choose/ .
	 *
	 * @param array $arr_sizes Array passed by the WP filter: 'image_size_names_choose'.
	 *
	 * @return array
	 */
	public function filterImageSizeNamesChoose( array $arr_sizes );
}
