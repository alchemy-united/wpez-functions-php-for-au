<?php

namespace WPezFunctionsPhpForAU\App;

use WPezFunctionsPhpForAU\App\Core\ThemeHeadCleanup\ClassThemeHeadCleanup as THC;
use WPezFunctionsPhpForAU\App\Core\ThemeHeadCleanup\ClassHooks as THChooks;

use WPezFunctionsPhpForAU\App\Core\ThemeAddSupport\ClassThemeAddSupport as TAS;
use WPezFunctionsPhpForAU\App\Core\ThemeAddSupport\ClassHooks as TAShooks;

use WPezFunctionsPhpForAU\App\Core\ImageSizeRegister\ClassImageSizeRegister as ISR;
use WPezFunctionsPhpForAU\App\Core\ImageSizeRegister\ClassHooks as ISRhooks;

use WPezFunctionsPhpForAU\App\Core\ImageSizeNamesChoose\ClassImageSizeNamesChoose as ISNC;
use WPezFunctionsPhpForAU\App\Core\ImageSizeNamesChoose\ClassHooks as ISNChooks;


class ClassPlugin {

	// protected $_arr_thc;
	protected $arr_tas;
	protected $arr_isr;


	public function __construct() {

		$this->setPropertyDefaults();

		$this->themeHeadCleanup( true );

		$this->themeAddSupport( true );

		$this->imageSizeRegister( true );

		$this->imageSizeNamesChoose( true );

		// We're using <br> in the title but need to replace those with a space in the RSS feed.
		$this->filterTitle( true );

	}

	protected function filterTitle( bool $bool = true ) {

		if ( true !== $bool ) {
			return;
		}
		add_filter( 'the_title', array( $this, 'theTitle' ), PHP_INT_MAX, 2 );
	}

	public function theTitle( $title, $id ) {

		// Only the titles in the RSS feeds.
		if ( is_feed() && strpos( $title, '<br>' ) !== false ) {
			return str_replace( '<br>', ' ', $title );
		}
		return $title;
	}

	protected function setPropertyDefaults() {
		// $this->_arr_thc = [];

		// tas = theme add support
		$this->arr_tas = array(

			'f1' => array(
				'active'  => false,
				'feature' => 'post-formats',
				'args'    => array(),
			),
			'f2' => array(
				'active'  => true,
				'feature' => 'post-thumbnails',
				'args'    => array( 'post', 'page' ),
			),
			'f3' => array(
				'active'  => true,
				'feature' => 'editor-color-palette',
				'args'    => array(
					array(
						'name'  => 'Red - #E11627',
						'slug'  => 'primary',
						'color' => '#E11627',
					),
					array(
						'name'  => 'White - #ffffff',
						'slug'  => 'white',
						'color' => '#ffffff',
					),
					array(
						'name'  => 'Black - #000000',
						'slug'  => 'black',
						'color' => '#000000',
					),
					/*
					[
						'name' => 'Green - #2FBC87',
						'slug' => 'secondary',
						'color' => '#2FBC87',
					],
					*/
					array(
						'name'  => 'Grey DK - #55555',
						'slug'  => 'accent-dark',
						'color' => '#55555',
					),
					array(
						'name'  => 'Grey XL - #C1C1C1',
						'slug'  => 'accent-light',
						'color' => '#C1C1C1',
					),
				),
			),
			'f4' => array(
				'active' => true,
				'feature' => 'responsive-embeds',
				'args' => array(),
			),
			'f5' => array(
				'active' => true,
				'feature' => 'disable-custom-font-sizes',
				'args' => array(),
			),
			'f6' => array(
				'active' => true,
				'feature' => 'disable-custom-colors',
				'args' => array(),
			),
		);

		// isr = image size register
		$this->arr_isr = array(

			'size1' => array(
				'name'         => 'wpez_xs',
				'width'        => 576,
				'height'       => 9999,
				'names_choose' => array(
					'option'                         => 'XS - 576w',
					'content_width_compare_override' => true,
				),
			),

			'size2' => array(
				'name'         => 'wpez_sm',
				'width'        => 768,
				'height'       => 9999,
				'names_choose' => array(
					'option'                         => 'SM - 768w',
					'content_width_compare_override' => true,
				),
			),

			'size3' => array(
				'name'         => 'wpez_md',
				'width'        => 992,
				'height'       => 9999,
				'names_choose' => array(
					'option'                         => 'MD - 992w',
					'content_width_compare_override' => true,
				),
			),

			'size4' => array(
				'name'         => 'wpez_lg',
				'width'        => 1200,
				'height'       => 9999,
				'names_choose' => array(
					'option'                         => 'LG - 1200w',
					'content_width_compare_override' => true,
				),
			),

			'size5' => array(
				'name'         => 'wpez_xl',
				'width'        => 1500,
				'height'       => 9999,
				'names_choose' => array(
					'option'                         => 'XL - 1500w',
					'content_width_compare_override' => true,
				),
			),

			'size6' => array(
				'name'         => 'wpez_xxl',
				'width'        => 1920,
				'height'       => 9999,
				'names_choose' => array(
					'option'                         => 'XXL - 1920w',
					'content_width_compare_override' => true,
				),
			)
		);
	}


	protected function themeHeadCleanup( $bool = true ) {

		if ( true !== $bool ) {
			return;
		}

		// https://gitlab.com/wpezsuite/WPezClasses/ClassThemeHeadCleanup
		$new   = new THC();
		$hooks = new THChooks( $new );
		$hooks->register();
	}

	protected function themeAddSupport( $bool = true ) {
		if ( true !== $bool ) {
			return;
		}

		$new = new TAS();
		$new->loadFeatures( $this->arr_tas );
		$hooks = new TAShooks( $new );
		$hooks->register();
	}

	protected function imageSizeRegister( $bool = true ) {

		if ( true !== $bool ) {
			return;
		}

		$new = new ISR();
		$new->loadImages( $this->arr_isr );
		$hooks = new ISRhooks( $new );
		$hooks->register();
	}

	protected function imageSizeNamesChoose( $bool = true ) {

		$new = new ISNC();
		$new->loadImages( $this->arr_isr );
		$new->setAddWhere( 'before' );
		$hooks = new ISNChooks( $new );
		$hooks->register();
	}

}
