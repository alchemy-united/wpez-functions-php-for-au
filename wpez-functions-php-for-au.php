<?php
/*
Plugin Name: WPezPlugins: Functions.php for Alchemy United
Plugin URI: https://gitlab.com/WPezPlugins/wpez-theme-functions-php-for-todo
Description: Typical theme functions.php stuff into a plugin (so it's persistent and portable).
Version: 0.0.5
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: https://AlchemyUnited.com
License: GPLv2 or later
Text Domain: TODO
*/
namespace WPezFunctionsPhpForAU;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

use WPezFunctionsPhpForAU\App\ClassPlugin;

$str_php_ver_comp = '5.4.0';

// we reserve the right to use traits :)
if ( version_compare( PHP_VERSION, $str_php_ver_comp, '<' ) ) {
	exit( sprintf( 'WPezPlugins: Theme Customize requires PHP ' . esc_html($str_php_ver_comp) . ' or higher. Your WordPress site is using PHP %s.', PHP_VERSION ) );
}


function autoloader( $bool = true ) {

	if ( true !== $bool ) {
		return;
	}

	require_once 'App/Core/Autoload/ClassAutoload.php';

	$new_autoload = new ClassAutoload();

	$new_autoload->setPathBase( plugin_dir_path( __FILE__ ) );
	$new_autoload->setNeedleRoot( __NAMESPACE__ );
	$new_autoload->setNeedleChild( 'App' );

	spl_autoload_register( [$new_autoload, 'wpezAutoload'], true );
}
autoloader();

function plugin( $bool = true ) {

	if ( true !== $bool ) {
		return;
	}

	$new_plugin = new ClassPlugin();
}
plugin();
