## WPezThemeCustomize

__Maintain WordPress theme customizations via a stand-alone plugin.__

### OVERVIEW

If you're using a child theme, imagine this plugin as a stand-alone layer that keeps "concerns" properly separated. How you configure the base (parent) theme should not be confused with how you extend it (via a child). Yes, they're related but none the less deserve to be free-standing.

If you're not using a child theme, this plugin will let you customize a theme without having to commit to a child (yet). 



> --
>
> IMPORTANT
>
> This is the _boilerplate_. That is, you're going to have to write some code. 
>
> -- 


__Please Note__

Be sure to update the namespace in each of the files. WPezThemeCustomize should be search & replaced with your own.  

Note: Technically, this isn't necessary if you're only going to have a single instance of the plugin activated at any one time.
  
That said, more than one means the plugin's folder will have to be changed / customized for each version beyond the initial one.

Pardon me if this is all stating the obvious.

### GETTING STARTED

Theme customizations are separated naturally into four parts:

1. Actions
2. Filters
3. Other
4. WPCore - _Probably not something you're going to edit_

You'll find a class for your code for each of these in their respective folders under App / Theme. 

The methods you'd define in these are controlled (i.e., via hooks) in App / Theme / ClassPlugin.php. Imagine - kinda sorta - ClassPlugin as a child theme's functions.php. 

As a rule of thumb, the classes under App/Theme/* define "The What". The ClassPlugin (uses those classes and) defines "The When." To make your "What" classes as reusable as possible, don't mix "The When" within "The What". 


### FAQ

- ?

### TODO

- ?


### CHANGE LOG

__-- 0.0.6__

- ADDED - functions-plugged.php 


__-- 0.0.5__

- CHANGED - Updated to the current version of ClassHooksRegister
- ADDED - WPezCore now has removeSettings() for removing theme options / settings (so you can hardcode those values with set_theme_mod())

__-- 0.0.4__

- CHANGED - restructured folder Core/RegisterHooks to Core/Hooks/Register
- CHANGED - other minor renamings etc to keep things consistent

__-- 0.0.3__

 - ADDED - autoloader
 - ADDED - ClassRegisterHooks
 - CHANGED - refactored / cleaned up ClassWPCore
 - CHANGED - refactored where necessary due to any of the above
   